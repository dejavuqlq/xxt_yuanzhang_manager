const path = require('path')

module.exports = {
    pluginOptions: { // 第三方插件配置
        'style-resources-loader': {
            preProcessor: 'less',
            patterns: [path.resolve(__dirname, './src/element-variables.less')] // less所在文件路径
        }
    },
    lintOnSave: false,
    publicPath: './',
    // 开启热更新
    chainWebpack: config => {
        config.resolve.symlinks(true)
        config.plugin('html').tap(args => {
            args[0].title = "校信通_园长端"; // 修改html 的title
            return args;
        })
    },
    // 开发配置项
    devServer: {
        open: true,
        host: 'localhost',
        port: 8810,
        https: false,
        hotOnly: false,
        proxy: { // 配置跨域
            '/garden': {
                //要访问的跨域的api的域名
                target: 'http://server.zhhost.top/',
                ws: true,
                changOrigin: true,
                pathRewrite: {
                    '^/garden': '/garden/'
                }
            }
        },
        before: () => {
        }
    },
    // css解析模式
    css: {
        loaderOptions: {
            // sass: {
            //     data: `@import "~@/element-variables.less";`
            // }
            less: {
                data: `@import "~@/element-variables.less";`
            }
        }
    },
    // configureWebpack: config => {
    //     config.externals = {
    //         "BMap": "BMap"
    //     }
    //   },

}