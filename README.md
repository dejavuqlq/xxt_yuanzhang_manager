# vuepro

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# vue-cli@4.5 开发配置
```javascript
+ vue create vuepro // 实例化项目
+ cd vuepro  // 进入项目
+ vue add router  // 引入路由
+ vue add typescript  // 引入ts
+ vue add axios  //-----这里的问题注意看plugins/axios.ts
// + vue add vuex  // 引入vuex 暂时没用到
+ vue add element-plus  // 使用element-plus

```

## 开发规则
 + 弹框/draw规则：编辑内容过少做成弹框的形式 / 内容过多draw
 +
