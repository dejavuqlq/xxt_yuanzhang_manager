// 配置文件域名
export const fileHost = () => {
    return "http://files.zhhost.top/"
};
// 本地未上传地址
export const beforeUpload = () => {
    return "http://server.zhhost.top/"
};
// 配置文件域名
export const uploadHost = () => {
    return "http://server.zhhost.top/zh8848/upload"
};
// 获取cookie
export const getCookie=(name:string)=>{
    let prefix = name + "="
    let start = document.cookie.indexOf(prefix)
    if (start == -1) {
        return null;
    }
    let end = document.cookie.indexOf(";", start + prefix.length)
    if (end == -1) {
        end = document.cookie.length;
    }
    let value = document.cookie.substring(start + prefix.length, end)
    return unescape(value);
};
// 快捷时间选项
export const shortcuts = () => {
    return [{
        text: '最近一周',
        value: (() => {
            const end = new Date();
            const start = new Date();
            start.setTime(start.getTime() - 3600 * 1000 * 24 * 7);
            return [start, end]
        })()
    }, {
        text: '最近一个月',
        value: (() => {
            const end = new Date();
            const start = new Date();
            start.setTime(start.getTime() - 3600 * 1000 * 24 * 30);
            return [start, end]
        })()
    }, {
        text: '最近三个月',
        value: (() => {
            const end = new Date();
            const start = new Date();
            start.setTime(start.getTime() - 3600 * 1000 * 24 * 90);
            return [start, end]
        })()
    }]
} ;
export const areaList = () => {
    return [{
        "name": "滨和社区",
        "value": "01",
        "sub": [{"name": "滨江人才公寓", "value": "0101"}, {"name": "钱塘滨和花园", "value": "0102"}, {
            "name": "科创公寓",
            "value": "0103"
        }]
    }, {
        "name": "滨兴社区",
        "value": "02",
        "sub": [{"name": "滨兴一号公寓", "value": "0201"}, {"name": "滨兴二号公寓", "value": "0202"}]
    }, {
        "name": "文艺社区",
        "value": "03",
        "sub": [{"name": "文艺一号公寓", "value": "0301"}, {"name": "文艺二号公寓", "value": "0302"}]
    }]
};
// 设置cookie
export const setCookie = (name: string, value: string, day: number = 30) => {
    let Days = 30;
    let exp = new Date();
    exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
    document.cookie = name + "=" + escape(value) + ";expires=" + exp.toUTCString();
};
// 获取cookie
export const delCookie = (name: string) => {
    let arr;
    let reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    // eslint-disable-next-line no-cond-assign
    if (arr = document.cookie.match(reg))
        return unescape(arr[2]);
    else
        return null;
};
// 获取地址栏参数
export const setectQuery = (name: string = '') => {
    let href: string = window.location.href;
    let strArr: Array<string> = href.split("?");
    if (strArr.length === 0) {
        return "没有参数"
    } else if (strArr.length > 2) {
        return "Error:发现多个 ？"
    } else {
        let queryArr: Array<string> = strArr[1].split("&"),
            obj: any = {};
        queryArr.map((item: string, index: number) => {
            let o: Array<string> = item.split("=");
            obj[o[0]] = o[1]
        })
        if (name) {
            return obj[name]
        } else {
            return obj
        }
    }
};
// 日期格式化
export const formatDate = (date:string='') => {
    if(date){
        let newDate = new Date(date);

        let year = newDate.getFullYear().toString();
        let month = (newDate.getMonth() +1).toString();
        let day = newDate.getDate().toString();
        if(month.length<2){
            month = '0'+ month;
        }
        if(day.length<2){
            day = '0'+ day;
        }
        return `${year}-${month}-${day}` ;
    }else{
        return ''
    }

};