import {createRouter, createWebHashHistory} from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Index from '../views/Index.vue'
import store from "../store";
// 默认路由 普通用户
const routes = [
    {path: '/', name: 'Login', component: Login},
    {
        path: '/Home', name: "Home", component: Home,
        children: [
            {path: "/Index", name: "Index", component: Index},
            {path: '/equipmentmanage', name: "equipmentmanage", component: () => import('../views/equipmentmanage/list.vue')},
            {path: '/checkonmmanage', name: "checkonmmanage", component: () => import('../views/checkonmmanage/list.vue')},
            {path: '/infomationmanage', name: "infomationmanage", component: () => import('../views/infomationmanage/list.vue')},
            {path: '/peoplemanage', name: "peoplemanage", component: () => import('../views/manageruser/list.vue')},
            {path: '/wxmanage', name: "wxmanage", component: () => import('../views/wxmanage/list.vue')},
            {path: '/classcardmanage', name: "classcardmanage", component: () => import('../views/classcardmanage/list.vue')},
            {path: '/librarymanage', name: "librarymanage", component: () => import('../views/librarymanage/list.vue')},
            {path: '/booksmanage', name: "booksmanage", component: () => import('../views/booksmanage/list.vue')},

        ]
    },

];
// 初始化 默认路由
const router = createRouter({
    history: createWebHashHistory(),
    routes
});
export default router
