import './plugins/axios'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// @ts-ignore
import axios from './plugins/axios.ts'
import installElementPlus from './plugins/element'
// import "element-variables.less"
const app = createApp(App)
installElementPlus(app)
app.use(store).use(router).use(axios).mount('#app')