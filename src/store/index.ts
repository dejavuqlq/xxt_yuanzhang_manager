import { createStore } from 'vuex'
import storage from './storage';
// url对应的tab的名字
const urlName = {
  "/tab1":"首页",
  "/tab2":'修改密码',
  "/tab3":'管理员管理',
  "/tab4":'导入用户',
  "/tab5":'用户管理',
  "/tab6":'图书列表',
};
export default createStore({
  state: {  // 单一状态树
    username:'', // 登录的用户名
    userid:'',
    power:"0", // 登录的用户的权限  0 普通用户  1 管理员  2 超级管理员
    token:'',  // 用户权限
    urlList:{  // 已打开的页面的数量
      "/tab1":1,   // 首页
    },
    urlName,
    menu:[], // 用户的权限菜单
    allPowerMenu:[] , // 所有的权限列表
    parentHeight:''
  },
  mutations: { // 更改 Vuex 的 store 中的状态
    // 更新urlList
    updateUrl(state, urlList){
      state.urlList = urlList
    },
    setUserName(state,username){
      state.username = username
    },
    setUserId(state,userid){
      state.userid = userid
    },
    setPower(state,power){
      state.power = power;
    },
    setToken(state,token){
      state.token = token;
    },
    setPH(state,parentHeight){
      state.parentHeight = parentHeight;
    },
    setMenu(state,menu){
      state.menu = menu;
    },
    setAllPowerMenu(state,allPowerMenu){
      state.allPowerMenu = allPowerMenu;
    }
  },
  getters:{
    getUserName(state){
      return 'my name is '+state.username
    },
    getMenu(state){
      return state.menu;
    },

  },
  actions: { // Action 提交的是 mutation，而不是直接变更状态。 可以包含任意异步操作。
    getUserInfo({ state,commit }){
      new Promise(resolve => {
          const num=Math.floor((Math.random()*10)+1);
          commit("setUserName",'lss00'+num);
          resolve();
      });
    },
    setPHAction({state,commit },val='0'){
      new Promise(resolve => {
        commit("setPH", val);
        resolve();
      })
    },
    updateUrl( {state,commit },obj={} ){  // obj 为传递过来的参数
      new Promise(resolve => {
        // console.log(2,obj)
        commit("updateUrl", obj);
        resolve();
      })
    },
    setUserNameAction( {state,commit },name=''){  // obj 为传递过来的参数
      new Promise(resolve => {
        commit("setUserName", name);
        resolve();
      })
    },
    // 更改用户权限
    setPowerAction({state,commit },val='0'){
      new Promise(resolve => {
        commit("setPower", val);
        resolve();
      })
    },
    // 更改用户id
    setUserIdAction({state,commit },val=''){
      new Promise(resolve => {
        commit("setUserId", val);
        resolve();
      })
    },
    // 用户token
    setTokenAction({state,commit },val='0'){
      new Promise(resolve => {
        commit("setToken", val);
        resolve();
      })
    },
    setMenuAction({state,commit },val=[]){
      new Promise(resolve => {
        commit("setMenu", val);
        resolve();
      })
    },
    // 所有的权限列表
    setAllPowerMenuAction({state,commit },val=[]){
      new Promise(resolve => {
        commit("setAllPowerMenu", val);
        resolve();
      })
    },
    loginOut({state,commit },val=''){
      new Promise(resolve => {
        commit("setAllPowerMenu", []);
        commit("setToken", '0');
        commit("setUserName", '');
        resolve();
      })
    }
  },
  modules: { // 将 store 分割成模块（module）
    storage: storage
  }
})
